Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Terminal Image Viewer
Upstream-Contact: Stefan Haustein <stefan.haustein@gmail.com>
                  Aaron Liu <aaronliu0130@gmail.com>
Source: <https://github.com/stefanhaustein/TerminalImageViewer>
Files-Excluded:
           src/CImg.h
           src/java/TerminalImageViewer.java

Files:     .github/ISSUE_TEMPLATE/bug_report.yml
           .github/ISSUE_TEMPLATE/config.yml
           .github/ISSUE_TEMPLATE/feature_request.yml
           .github/pull_request_template.md
           .gitignore
           README.md
           snapcraft.yaml
           src/Makefile
           src/tiv.cpp
           terminalimageviewer.rb
Copyright: Copyright (c) 2017-2023, Stefan Haustein, Aaron Liu
License:   GPL-3+ or Apache-2.0

Files: src/tiv.cpp
Copyright: Copyright (c) 1987, 1993
 * The Regents of the University of California.  All rights reserved.
License: BSD-3-clause
Comment:
 A small portion of the aforementioned file includes source code
 under the above copyright and license. The block of code is marked
 with a comment in the source. The rest of this file is under the
 main dual Apache-2.0/GPL-3+ license and copyright of Stefan
 Haustein and Aaron Liu.

Files:     debian/*
Copyright: Copyright (c) 2024 Loren M. Lang
License:   GPL-3+ or Apache-2.0

License: GPL-3+
 On Debian systems the full text of the GNU General Public
 License can be found in the `/usr/share/common-licenses/GPL-3'
 file.

License: Apache-2.0
 On Debian systems the full text of the GNU General Public
 License can be found in the `/usr/share/common-licenses/Apache-2.0'
 file.

License: BSD-3-clause
 On Debian systems the full text of the BSD license can be
 found in the `/usr/share/common-licenses/BSD' file.
